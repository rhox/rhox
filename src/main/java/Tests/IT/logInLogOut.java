package Tests.IT;

import Tests.ASSTest;
import Tests.Helper.Helper;
import Tests.PageObjects.LoginPage;
import Tests.PageObjects.TopMenuPage;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;




@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class logInLogOut extends ASSTest {


    /**
     * Class declaration
     */
    private LoginPage loginPage = new LoginPage(driver);
    private Helper helper = new Helper(driver);
    private TopMenuPage topMenu = new TopMenuPage(driver);



    /**
     * Methods
     */
    @Test
    public void logIn() throws InterruptedException {


        loginPage.enter_login.sendKeys("test01");
        loginPage.enter_password.sendKeys("Qwerty123");
        loginPage.button_log_in.click();

        helper.waitForVisibilityOfText("a", "Log Out");

        Assert.assertEquals(driver.findElement(By.xpath("//a[contains(text(),'Update Profile')]")).getText(), "Update Profile");

        
    }





    @Test()
    public void logOut() {

        topMenu.logOut.click();
        helper.waitForVisibilityOfText("a", "Log In");


        Assert.assertEquals(driver.findElement(By.xpath("//a[contains(text(),'Log In')]")).getText(), "Log In");


    }




}
