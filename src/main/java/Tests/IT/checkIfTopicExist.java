package Tests.IT;

import Tests.ASSTest;
import Tests.Helper.Helper;
import Tests.PageObjects.LoginPage;
import Tests.PageObjects.TopMenuPage;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Arek on 2016-06-27.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class checkIfTopicExist extends ASSTest {


    /**
     * Class declaration
     */
    private static WebDriverWait wait;
    private LoginPage loginPage = new LoginPage(driver);
    private Helper helper = new Helper(driver);
    private TopMenuPage topMenu = new TopMenuPage(driver);
    int row0;



    /**
     * Methods
     */
    @Test
    public void LogIn() throws InterruptedException {


        loginPage.enter_login.sendKeys("test01");
        loginPage.enter_password.sendKeys("Qwerty123");
        loginPage.button_log_in.click();

        helper.waitForVisibilityOfText("a", "Log Out");

        Assert.assertEquals(driver.findElement(By.xpath("//a[contains(text(),'Update Profile')]")).getText(), "Update Profile");
    }


    @Test
    public void yExistYet() {


        driver.findElement(By.xpath("//a[contains(text(),'CHECKBOXES')]")).click();
        helper.waitForVisibilityOfText("a", "checkbox");
        Assert.assertEquals(driver.findElement(By.xpath("//h1[@id='this_topic_does_not_exist_yet']")).getText(), "This topic does not exist yet");



        driver.findElement(By.xpath("//a[contains(text(),'DROPDOWNS')]")).click();
        helper.waitForVisibilityOfText("a", "dropdowns");
        Assert.assertEquals(driver.findElement(By.xpath("//h1[@id='this_topic_does_not_exist_yet']")).getText(), "This topic does not exist yet");


        driver.findElement(By.xpath("//a[contains(text(),'ROLLUPS')]")).click();
        helper.waitForVisibilityOfText("a", "rollups");
        Assert.assertEquals(driver.findElement(By.xpath("//h1[@id='this_topic_does_not_exist_yet']")).getText(), "This topic does not exist yet");

    }
}
