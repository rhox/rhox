package Tests.IT;

import Tests.ASSTest;
import Tests.Helper.Helper;
import Tests.PageObjects.LoginPage;
import Tests.PageObjects.TopMenuPage;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;





@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LogInScrollDown extends ASSTest {

    /**
     * Class declaration
     */
    private static WebDriverWait wait;
    private LoginPage loginPage = new LoginPage(driver);
    private Helper helper = new Helper(driver);
    private TopMenuPage topMenu = new TopMenuPage(driver);

    Long value2;



    /**
     * Methods
     */
    @Test
    public void logIn() throws InterruptedException {


        loginPage.enter_login.sendKeys("test01");
        loginPage.enter_password.sendKeys("Qwerty123");
        loginPage.button_log_in.click();

        helper.waitForVisibilityOfText("a", "Log Out");

        Assert.assertEquals(driver.findElement(By.xpath("//a[contains(text(),'Update Profile')]")).getText(), "Update Profile");

    }


    @Test
    public void scrollDown() throws InterruptedException {


        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,250)", "");


        JavascriptExecutor js2 = (JavascriptExecutor)driver;
        js2.executeScript("window.scrollBy(0, -250)", "");


        JavascriptExecutor executor = (JavascriptExecutor) driver;
        value2 = (Long) executor.executeScript("return window.scrollY;");


        Assert.assertEquals( (long) value2, 0);

    }

}
