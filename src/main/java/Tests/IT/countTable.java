package Tests.IT;

import Tests.ASSTest;
import Tests.Helper.Helper;
import Tests.PageObjects.LoginPage;
import Tests.PageObjects.TopMenuPage;
import org.junit.Assert;


import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;





@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class countTable extends ASSTest {

    /**
     * Class declaration
     */
    private static WebDriverWait wait;
    private LoginPage loginPage = new LoginPage(driver);
    private Helper helper = new Helper(driver);
    private TopMenuPage topMenu = new TopMenuPage(driver);
    int row0;



    /**
     * Methods
     */
    @Test
    public void LogIn() throws InterruptedException {


        loginPage.enter_login.sendKeys("test01");
        loginPage.enter_password.sendKeys("Qwerty123");
        loginPage.button_log_in.click();

        helper.waitForVisibilityOfText("a", "Log Out");

        Assert.assertEquals(driver.findElement(By.xpath("//a[contains(text(),'Update Profile')]")).getText(), "Update Profile");
    }



    @Test
    public void TableCount() {


        helper.waitForVisibilityOfText("a", "TABLE");

        driver.findElement(By.xpath("//a[contains(text(),'TABLE')]")).click();



        helper.waitForVisibilityOfText("h1", "TABLES");


        Assert.assertEquals(driver.findElements(By.xpath(".//*[@class='row0']")).size(), 1);
        Assert.assertEquals(driver.findElements(By.xpath(".//*[@class='row1']")).size(), 1);
        Assert.assertEquals(driver.findElements(By.xpath(".//*[@class='row2']")).size(), 1);


        Assert.assertEquals(driver.findElements(By.xpath(".//*[@class='row0']/th")).size(), 3);
        Assert.assertEquals(driver.findElements(By.xpath(".//*[@class='row1']/td")).size(), 3);
        Assert.assertEquals(driver.findElements(By.xpath(".//*[@class='row2']/td")).size(), 3);



    }



}
