package Tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.MalformedURLException;


public class ASSTest {

    protected static WebDriver driver;


    @BeforeClass
    public static void beforeClass() throws MalformedURLException {

        System.setProperty("webdriver.chrome.driver", "C:/Users/Arek/Downloads/POMExample/chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();

        driver.get("http://testpage.2bqa.net/doku.php");
    }


    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
