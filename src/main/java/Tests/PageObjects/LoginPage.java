package Tests.PageObjects;


import Tests.Helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


/**
 * Created by Damian Wojtaszek on 2016-03-03.
 */
public class LoginPage {

    /**
     * Locators
     */
    @FindBy(how = How.ID, using = "focus__this")
    public static WebElement enter_login;

    @FindBy(how = How.NAME, using = "p")
    public static WebElement enter_password;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Log In')]")
    public static WebElement button_log_in;


    private WebDriver driver;
    private Helper helper;


    /**
     * Constructor
     */
    public LoginPage(WebDriver driver) {
        this.driver = driver;
        helper = new Helper(driver);
        PageFactory.initElements(driver, this);
    }


    /**
     * Getters
     */
    public static WebElement getButton_log_in() {
        return button_log_in;
    }

    public static WebElement getEnter_password() {
        return enter_password;
    }

    public static WebElement getEnter_login() {
        return enter_login;
    }




}