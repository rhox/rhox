package Tests.PageObjects;


import Tests.Helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


/**
 * Created by Damian Wojtaszek on 2016-03-03.
 */
public class TopMenuPage {

    /**
     * Locators
     */
    @FindBy(how = How.XPATH, using = ".//a[contains(text(),'Update Profile')]")
    public static WebElement updateProfile;


    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Log Out')]")
    public static WebElement logOut;


    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Log In')]")
    public static WebElement logIn;


    private WebDriver driver;
    private Helper helper;



    /**
     * Constructor
     */
    public TopMenuPage(WebDriver driver) {
        this.driver = driver;
        helper = new Helper(driver);
        PageFactory.initElements(driver, this);
    }


    /**
     * Getters
     */
    public static WebElement getLogIn() {
        return logIn;
    }

    public static WebElement getLogOut() {
        return logOut;
    }

    public static WebElement getUpdateProfile() {
        return updateProfile;
    }




}