package Tests.Helper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Damian Wojtaszek on 2016-03-09.
 */
public class Helper {

    /**
     * Class declaration
     */
    private static WebDriver driver;
    private static WebDriverWait wait;
    private static WebDriverWait shortWait;

    /**
     * Conctructor
     */
    public Helper(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 15, 100);
        this.shortWait = new WebDriverWait(driver, 2, 100);
    }

    /**
     * Methods
     */
    public static void waitForVisibilityOfText(String path, String text) {

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//" + path + "[contains(text(),'" + text + "')]")));
    }


}